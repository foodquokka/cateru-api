<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyProfile;

use DB;

class CompanyProfileController extends Controller
{
    public function newCompany(){
        $allCompanies = CompanyProfile::All();

        return response()->json([
            'allCompanies' => $allCompanies
        ]);
    }

    public function addNewCompany(Request $request){
        $newCompany= new CompanyProfile();
        
        $newCompany->companyname = $request->companyname;
        $newCompany->address = $request->address;
        $newCompany->tin = $request->tin;
        $newCompany->contactNo = $request->contactNo;
        $newCompany->email = $request->email; 

        $newCompany->save();

        return response()->json([
            'message' => "Saved successfully!"
        ]);
  }
  public function getCompanyProfileByID($id){
    $info = CompanyProfile::find($id);

    return response()->json([
        'info' => $info
    ]);
  }
  public function getCompanyProfileByName($name){
    $info = DB::table('companies')->where('companyname','=',$name)->get();

    return response()->json([
        'info' => $info
    ]);
  }
  public function getCompanyProfile(){
    $info = CompanyProfile::All();

    return response()->json([
        'info' => $info
    ]);
  }

}
