<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use App\RestaurantTable;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Exceptions\CustomExceptions;
use DB;
class TableController extends BaseController
{
    private $customExceptions;

    public function __construct(CustomExceptions $customExceptions)
    {
        $this->customExceptions = $customExceptions;
    }
    public function tableList(){
       $allTables = RestaurantTable::all();
        return response()->json(['allTables'=> $allTables]);
    }

    public function webTableList(){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $userImage=$user->image;
        $allTables = RestaurantTable::all();
         return view('admin.tablelist',compact('userImage','allTables','userFname','userLname'));
     }


    public function getAvailableTable(){
        $availableTables = DB::table('tables')->where('status','Available')->get();

        return response()->json([
            'AvailableTables' => $availableTables
        ]);
    }
    public function getOccupiedTable(){
        $occupiedTables = DB::table('tables')->where('status','Occupied')->get();

        return response()->json([
            'OccupiedTables' => $occupiedTables
        ]);
    }

     public function newTable(){
    $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $userImage=$user->image;
        return view('addtables',compact('userImage','userFname','userLname'));

    }
    public function addTable(Request $request){
        try{
            $table=$this->customExceptions->AddTable($request);
          }
          catch(\PDOException $e){
            return back()->withError($e->getMessage())->withInput();
          }
        $table = new RestaurantTable;
        $table->tableno=$request->tablenum;
        $table->capacity = $request->capacity;
        $table->status = $request->status;

        $table->save();

        return redirect('/table/tablelist')->with('success','Table Successfully Created')->withInput();
    }

public function removeTable($tableno){
    $Table= RestaurantTable::find($tableno);

    if ($Table) {
        $Table->delete();
    }

    return redirect()->to(url('/table/tablelist'));
}
public function editTable(Request $request,$tableno=null){
    $user = Auth::user();
    $userFname=$user->empfirstname;
    $userLname=$user->emplastname;
    $userImage=$user->image;
    $tables = RestaurantTable::where(['tableno'=>$tableno])->first();
   if($request->isMethod('post')){
       $data= $request->all();
       try{
        $table=$this->customExceptions->EditTable($request);
      }
      catch(\PDOException $e){
        return back()->withError($e->getMessage())->withInput();
      }
       RestaurantTable::where(['tableno'=>$tableno])
        ->update(['tableno'=>$data['tablenum'],'capacity' =>$data['capacity'],'status'=>$data['status']]);

        return redirect('/table/tablelist')->with('success','Table Successfully Updated');
   }
  // dd($currentCategory->categoryid);
 return view('edit_tables')->with(compact('userImage','tables','userFname','userLname'));
}
    public function setTableStatus(Request $request){
        $table = RestaurantTable::find($request->tableno);
        $table->status = $request->status;
        $table->save();

        return response()->json([
            'message' => 'Table is set'
        ]);
    }

    public function setTableAvailable($tableno){
        $table = RestaurantTable::find($tableno);
        $table->status = 'Available';
        $table->save();

        return response()->json([
            'message' => 'Table is set to available'
        ]);
    }
    public function setTableOccupied($tableno){
        $table = RestaurantTable::find($tableno);
        $table->status = 'Occupied';
        $table->save();

        return response()->json([
            'message' => 'Table is set to occupied'
        ]);
    }
   

    public function clearTable(Request $request){
        $table = RestaurantTable::find($request->tableno);
        $table->status = 'Available';
        $table->save();

        return response()->json([
            'message' => 'Table Cleared'
        ]);
    }


    public function setDeviceTable(Request $request){
            $message = '';

            $table=RestaurantTable::find($request->tableno);
            if($table->status != 'Occupied'){
                $table->deviceuid = $request->deviceuid;
                $table->save();

                $message = "Table is set";
            } else {
                $message = "Table is occupied";
            }



            return response()->json([
                'message' => $message
            ]);
        }

    public function getDeviceTableNo($deviceuid){
        $t;
        $table = DB::table('tables')
                ->where('deviceuid',$deviceuid)->get();

        foreach($table as $tableno){
            //array_push($t, array(
                $t = $tableno->tableno;
           // ));
        }
        return response()->json([
            'tableno' => $t
        ]);
    }
    public function getTableStatus($tableno){
        $status ='';
        $table = RestaurantTable::find($tableno);
        if($table->status == 'Available'){
            $status = 'Available';
        } else{
            $status = 'Occupied';
        }

        return  $status;
     
    }

    public function tableTransfer(Request $request){

        $orders = DB::table('orders')
                ->where('tableno',$request->transferfrom)
                ->where('status','ordering')->get();
        if($orders != ""){
            $orders = DB::table('orders')
            ->where('tableno',$request->transferfrom)
            ->update(['tableno' => $request->transferto]);
            self::setTableAvailable($request->transferfrom);
            self::setTableOccupied($request->transferto);
        }
        return response()->json([
            'message' => 'Transfer Success!'
        ]);
    }
    public function mergeTables(Request $request){

        $dataArray=[];
        $data = $request->All();
        foreach($data as $table){
            array_push($dataArray,$table);
        }
        $max =  max($dataArray);
        
        $table = RestaurantTable::find($max);
        $table->status= 'Occupied';
        $table->save();
       
        return response()->json([
           'message' => 'Merging success!'
        ]);
    }
    public function getTableStatusNotPaid(){
        $tables = DB::table('orders')->where('status','!=','paid')->get();
        
        return response()->json([
            'tables' => $tables
        ]);
    }


    public function getOrderByTableNoWaiter($tableno){
        $table = RestaurantTable::find($tableno);
            if($table->status === 'Occupied'){
                 $orders = DB::table('kitchenrecords')
                ->select('menus.name','kitchenrecords.status','kitchenrecords.orderQty','kitchenrecords.id'
                ,'menus.price','orders.order_id')
                ->join('orders','orders.order_id','=','kitchenrecords.order_id')
                ->join('menus','menus.menuID','=','kitchenrecords.menuID')
                ->where('orders.tableno',$tableno)
                // ->Where('kitchenrecords.status','!=','served')
                ->orderBy('kitchenrecords.date_ordered','asc')
                ->get();
            }

         return response()->json([
             'orders' => $orders, 
         ]);
    
            
    }
    
    public function getOrderByTableNo($tableno){
        $order_id;
        $isEmpty = true;
        $orders = DB::table('tables')
                ->select('orders.order_id')
                ->join('orders','orders.tableno','=','tables.tableno')
                ->where('orders.status','!=','paid')
                ->where('tables.tableno',$tableno)
                ->where('tables.status','Occupied')
                ->get(); 
     
            foreach($orders as $order){
                    $order_id = $order->order_id;
                    $isEmpty = false;
            }
            if($isEmpty) $order_id='';

        return response()->json([
            'order_id' => $order_id
        ]);  
    }


    public function getCartItems($order_id){
        $items = DB::table('carts')->get();

        return response()->json([
            'items' => $items
        ]);
        
    
            
    }

}
