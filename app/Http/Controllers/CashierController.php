<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use App\Order;
use App\Menu;
use DB;
class CashierController extends Controller
{
    public function getbilldetail($tableno){
        $send=[];

        $name ="";
        $price = "";
        $orders = DB::table('order_details')
        ->select(
        'menus.name AS menuname',
        'menus.price AS price'
        ,'orders.order_id',
        'tableno',
        'orders.total AS total',
        'order_details.orderQty AS orderNum',
        'order_details.qtyServed AS served',
        'order_details.subtotal AS subtotal',
        'order_details.status AS status',
        'order_details.id AS id',
        'order_details.bundleid AS bundleid',
        'orders.custid AS custid',
        'orders.order_id AS order_id'
        )
        ->join('orders','orders.order_id','=','order_details.order_id') 
        ->join('menus','menus.menuID','=','order_details.menuID')
        ->where('orders.tableno',$tableno)
        ->where('orders.status','billout')
        ->where('order_details.status','!=','cancelled')
        ->get();

        foreach($orders as $row){
            $bundlemenus = DB::table('bundle_menus')->where('bundleid',$row->bundleid)->get();
            foreach($bundlemenus as $data){
                $name = $data->name;
                $price = $data->price;
            }
           array_push($send,array(
             'bundleid' => $row->bundleid,
             'name' => $row->menuname,
            'bundlename' => $name,
            'bundleprice'=> $price,
            'custid' => $row->custid,
             'subtotal' => $row->subtotal,
             'status' =>  $row->status,
             'served' => $row->served,
             'orderNum'=> $row->orderNum,
             'tableno' => $row->tableno,
             'order_id'=>$row->order_id,
             'total' => $row->total,
             'id' => $row->id,
             'price' => $row->price,
           ));
          }
          
          // $final = array_unique($send);
         return response()->json([
            'records'=>$send
        // 'name'=>$name,
        // 'bundleid'=>$bundleid

         ]);

    }
    public function getreceiptdetail($order_id){
        $send=[];

        $name ="";
        $price = "";
        $orders = DB::table('order_details')
        ->select(
        'menus.name AS menuname',
        'menus.price AS price'
        ,'orders.order_id',
        'tableno',
        'orders.total AS total',
        'order_details.orderQty AS orderNum',
        'order_details.qtyServed AS served',
        'order_details.subtotal AS subtotal',
        'order_details.status AS status',
        'order_details.id AS id',
        'order_details.bundleid AS bundleid',
        'orders.custid AS custid',
        'orders.order_id AS order_id'
        )
        ->join('orders','orders.order_id','=','order_details.order_id') 
        ->join('menus','menus.menuID','=','order_details.menuID')
        ->where('orders.order_id',$order_id)
        ->where('orders.status','paid')
        ->where('order_details.status','!=','cancelled')
        ->get();

        foreach($orders as $row){
            $bundlemenus = DB::table('bundle_menus')->where('bundleid',$row->bundleid)->get();
            foreach($bundlemenus as $data){
                $name = $data->name;
                $price = $data->price;
            }
           array_push($send,array(
             'bundleid' => $row->bundleid,
             'name' => $row->menuname,
            'bundlename' => $name,
            'bundleprice'=> $price,
            'custid' => $row->custid,
             'subtotal' => $row->subtotal,
             'status' =>  $row->status,
             'served' => $row->served,
             'orderNum'=> $row->orderNum,
             'tableno' => $row->tableno,
             'order_id'=>$row->order_id,
             'total' => $row->total,
             'id' => $row->id,
             'price' => $row->price,
           ));
          }
          
          // $final = array_unique($send);
         return response()->json([
            'records'=>$send
        // 'name'=>$name,
        // 'bundleid'=>$bundleid

         ]);
    }
    public function updateTotal($order_id,Request $request){

        $orders = Order::find($order_id);
        $orders->total = $request->total;
        $orders->save();

        return response()->json([
            'message' => 'Bill sent'
        ]);

    }

    public function getBillOutList(){
         $orders = DB::table('orders')->where('status','billout')->get();

          return response()->json([
             'table' => $orders
          ]);
    }
    public function sendbill(Request $request){
        $bill = Order::find($request->order_id);
        $bill->status = $request->status;
        $bill->save();

        return response()->json([
           'message' => "Bill sent"
        ]);
    }
    public function getTotal($orderid){
        $data = DB::table('orders')
                ->select('total')
                ->where('order_id',$orderid)
                ->get();
        foreach($data as $d){
            $total = $d->total;
        }
        return response()->json(
         $total
        );
    }
  
    public function getCashTendered($orderid){
        $data = DB::table('orders')
                ->select('cashTender')
                ->where('order_id',$orderid)
                ->get();
        foreach($data as $d){
            $cash = $d->cashTender;
        }
        return response()->json(
         $cash
        );
    }
    public function getChange($orderid){
        $data = DB::table('orders')
                ->select('change')
                ->where('order_id',$orderid)
                ->get();
        foreach($data as $d){
            $change = $d->change;
        }
        return response()->json(
         $change
        );
    }
  
}
