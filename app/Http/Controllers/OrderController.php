<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\BaseController as BaseController;
use App\OrderDetail;
use App\Order;
use App\Menu;
use App\RestaurantTable;
use App\Customer;
use DB;
class OrderController extends BaseController
{
    public function newOrder($id){
        $allOrders = OrderDetail::find($id);
        $allMenus = Menu::all();
        $allSubCategories = SubCategory::all();
        $allCategories = Category::all();

        return response()->json([
            'allOrders' =>  $allOrders,
            'allMenus'  =>  $allMenus,
            'allSubCategories'  =>  $allSubCategories,
            'allCategories' => $allCategories
        ]);
    }

    public function startOrder(Request $request){

        $table = RestaurantTable::find($request->tableno);
        $table->status = 'Occupied';
        $table->save();

        $newCustomer = Customer::create(['name'=>'cash']);
        $newOrder = new Order;
        $newOrder->custid= $newCustomer->custid;
        $newOrder->empid=$request->empid;
        $newOrder->tableno = $table->tableno;
        $newOrder->status = 'ordering';
        $newOrder->total = 0;
        $newOrder->save();

        return response()->json([
            'message' => 'Welcome, happy eating!',
            'order_id' =>  $newOrder->order_id,
            'tableno' => $newOrder->tableno
        ]);

}

    public function paidOrder(){
        $menus = Menu::all();
        $details = OrderDetail::whereDate('date_ordered', '>=', Carbon::today()->toDateString());

        $paidOrders= Order::where('status','Paid')->get();
       // return view('admin.report.orderlist', compact('paidOrders','menus','details'));

       return response()->json([
           'details' => $details
       ]);
    }

    public function confirmPayment(Request $request){
        $orderItem = Order::find($request->order_id);
        $orderItem->status = 'billout';
        $orderItem->save();
        return response()->json([
            'message' => 'Bill out confirmed'
        ]);
    }

    public function successfulTransaction(){
        $details = OrderDetail::whereDate('date_ordered', '>=', Carbon::today()->toDateString());

        $paidOrders= Order::where('status','Paid')->get();
        return view('admin.report.sales', compact('paidOrders','details'));

    }
    public function getLatestOrderID($tableno){
        $order;
        $orderid = DB::table('orders')->select('order_id')
                    ->where('tableno', $tableno)->get();

        foreach($orderid as $od){
            if($od->status != 'billout'){
                $order = $od->order_id;
            }
        }

        return response()->json([
            'orderid' => $order
        ]);
    }
 
    public function setTotal(Request $request){
    
        $orders  = DB::table('orders')
                    ->select('total')
                    ->where('order_id', $request->order_id)
                    ->get();
        foreach($orders as $order){
            if($order->total === 0.00){
               $total = $request->total;
            }
            else {
                $total =$order->total + $request->total;
            }
        }
        $orderTotal = Order::find($request->order_id);
        $orderTotal->total = $total;
        $orderTotal->save();

        return response()->json([
            'message' => $total
        ]);
    }
    public function decreaseTotal($id){

        $orders  = DB::table('order_details')
                    ->select(
                        'order_details.status AS status',
                        'orders.total AS total',
                        'order_details.subtotal AS subtotal',
                        'order_details.order_id AS order_id')
                    ->join('orders','orders.order_id','=','order_details.order_id')
                    ->where('order_details.id', $id)
                    ->get();
        if($orders){
            foreach($orders as $order){
                $total = $order->total - $order->subtotal; 
                $order_id = $order->order_id;
            }
        $orderTotal = Order::find($order_id);
        $orderTotal->total = $total;
        $orderTotal->save();
       
        $orderStatus = OrderDetail::find($id);
        $orderStatus->status = 'cancelled';
        $orderStatus->save();
        }
     

        return response()->json([
            'message' => 'Order cancelled!'
          
        ]);
    }
   

}
