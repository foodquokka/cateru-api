<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Customer;
//se App\Traits\SMSMethod;

class SMS extends Controller
{
   // use SMSMethod;
    public function sendSMS($custid){

        $number;
      
        $phonenumber = DB::table('customers')
                        ->select('phonenumber')
                        ->where('custid',$custid)->get();

        foreach($phonenumber as $num){
            $number = $num->phonenumber;
        }

        $customer = Customer::find($custid);
        $customer->status='notified';
        $customer->save();
                        
        $query_string = 'api.aspx?apiusername='.'APIPEG60E2N2X'.'&apipassword='.'APIPEG60E2N2XPEG60';
        $query_string .= '&senderid='.rawurlencode('TEST').'&mobileno='.rawurlencode($number);
        $query_string .= '&message='.rawurlencode(stripslashes('Your table is ready. Please be present before 5 minutes.')) . "&languagetype=1";        
        $url ='http://gateway.onewaysms.ph:10001/'.$query_string;
        
        $fd = implode('',file($url));      
        if ($fd)  
        {                       
        if ($fd > 0) {
    //	Print("MT ID : " . $fd);
        $ok = "success!";
        }        
        else {
    //	print("Please refer to API on Error : " . $fd);
        $ok = "fail";
        }
            }           
            else      
            {                       
                // no contact with gateway                      
                        $ok = "failed";       
            }           
            return $ok;  
  //  return response()->json( $number);
    
    }  
    public function test(Request $request){
        $table = DB::table('tables')
        ->where('status','Available')
        ->where('capacity','>=',$request->capacity)->get();

        return response()->json([
            'table' => $table
        ]);
    }
        
}
