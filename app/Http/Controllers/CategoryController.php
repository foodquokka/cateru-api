<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\SubCategory;
use DB;
class CategoryController extends BaseController
{
    public function getCategoryName(){
        $allCategories= Category::all();
        $allSubs = SubCategory::all();
        return response()->json([
            'allCategories' => $allCategories,
            'allSubs'   => $allSubs,
        ]);
    }
    public function newCategory(){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $allCategories = Category::all();

        return view('admin.category.add_category',compact('allCategories','userFname','userLname'));
    }
    public function newSubCategory(){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $allCategories  = Category::all();
        return view('admin.category.add_subcategory',compact('userFname','userLname','allCategories'));
    }
    public function categoryList(Request $request){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $allCategories = Category::all();
            return view('admin.category.categorylist', compact('allCategories','userFname','userLname'));


    }
    public function apiCategoryList(Request $request){
        $allCategories = Category::all();
           // return view('admin.category.categorylist', compact('allCategories'));
        return response()->json([
            'allcategories' => $allCategories
        ]);

    }
    public function subcategoryList(Request $request){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $allSubCategories = SubCategory::all();
        $allCategories = Category::all();
            return view('admin.category.subcategorylist', compact('allSubCategories','allCategories','userFname','userLname'));


    }
    public function addCategory(Request $request){
        /**if mmode = add */
            $category = new Category;
            $category->categoryname = $request->categoryname;
            $category->description = $request->description;
            $category->save();

       return redirect()->to('/admin/view_categories');
       /** if mode = select */
    }

    public function editCategory(Request $request, $id=null){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
       if($request->isMethod('post')){
           $data= $request->all();

           Category::where(['categoryid'=>$id])
            ->update(['categoryname'=>$data['categoryname'],'description' => $data['description']]);

           return redirect('/admin/view_categories');
       }
       $categoryDetails = Category::where(['categoryid'=> $id])->first();
       return view('admin.category.edit_category')->with(compact('categoryDetails','userFname','userLname'));
    }
    public function deleteCategory($category_id)
    {
        $category = Category::find($category_id);
        if($category){
            $category->delete();
        }
        return redirect()->to('/admin/view_categories');
    }

    public function addSubCategory(Request $request){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $sub = new SubCategory;
        $sub->subname = $request->subname;
        $sub->categoryid = $request->categoryid;
        $sub->save();

   return redirect()->to('/admin/view_subcategories');
}
public function editSubCategory(Request $request,$id=null){
    $user = Auth::user();
    $userFname=$user->empfirstname;
    $userLname=$user->emplastname;
   if($request->isMethod('post')){
       $data= $request->all();
       SubCategory::where(['subcatid'=>$id])
        ->update(['subname'=>strtoupper($data['subname']),'categoryid' =>strtoupper($data['category'])]);

       return redirect('/admin/view_subcategories');
   }
   $subcategoryDetails = SubCategory::where(['subcatid'=> $id])->first();
   $currentCategory=Category::where(['categoryid'=>$subcategoryDetails['categoryid']])->first();
   $allCategories  = Category::all();
  // dd($currentCategory->categoryid);
 return view('admin.category.edit_subcategory')->with(compact('currentCategory','allCategories','subcategoryDetails','userFname','userLname'));
}
public function deleteSubCategory($subcategoryid)
{
    $subcategory = SubCategory::find($subcategoryid);
    if($subcategory){
        $subcategory->delete();
    }
    return redirect()->to('/admin/view_subcategories');
}
}
