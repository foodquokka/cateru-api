<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Kitchen;
use App\BundleDetails;
use App\OrderDetail;

use DB;

class TemporaryTableController extends Controller
{
    public function saveCart(Request $request){
        $data = $request->all();
        $finalArray = [];
        array_push($finalArray, $data);
          
        Cart::insert($finalArray);

        return response()->json([
            'final' => $finalArray
        ]);
    }
    public function saveCartBundle(Request $request){ //bugged
        // $data = $request->all();
         $finalArray = [];

       // $result = BundleDetails::find($request->bundleid);

        $result = DB::table('bundle_details')->where('bundleid',$request->bundleid)->get();
        foreach($result as $res){
           
            array_push($finalArray, array(
                'order_id'=> $request->order_id,
                'qty' => $request->qty,
                'menuID' =>  $res->menuID,
                'bundleid' => $res->bundleid
            ));
        }
        
        Cart::insert($finalArray);

        return response()->json(
            $result
        );
    }

   
    public function removeItemsbyOrderId($order_id){
       $orderid = DB::table('carts')->where('order_id',$order_id)->delete();
        return response()->json([
            'message' => 'Successfully deleted!'
          
        ]);
    }
    public function removeItemsbyId($id){
        $items = DB::table('carts')
                ->where('id',$id)->delete();

        return response()->json([
            'message' => 'Item successfully deleted!'
        ]);
    }
    public function removeItemByBundleID($bundleid){      

        $items = DB::table('carts')
                ->where('bundleid', $bundleid)->delete();

        return response()->json([
            'message' => 'Bundle ['.$bundleid.'] successfully deleted!'
        ]);        
    }

    public function getBundleItems($order_id){
        $send = [];  
        $items=[];
        $dataArray = [];
        $cartBundleItems =  DB::table('carts')
                        ->join('menus', 'carts.menuID', '=', 'menus.menuID')
                        ->join('bundle_menus','bundle_menus.bundleid','=','carts.bundleid')
                        ->selectRaw('group_concat(bundle_menus.bundleid) as bundleid')
                        ->selectRaw('group_concat(bundle_menus.name) as bundlename')
                        ->selectRaw('group_concat(bundle_menus.price) as price')
                        ->selectRaw('group_concat(menus.name) as name')
                        ->selectRaw('group_concat(carts.qty) as qty')
                        ->selectRaw('group_concat(carts.order_id) as order_id')
                        ->selectRaw('group_concat(carts.id) as id')
                        ->selectRaw('group_concat(carts.menuID) as menuID')
                        ->where('order_id', $order_id)
                        ->groupBY('bundle_menus.bundleid')
                        ->get();  

                        foreach($cartBundleItems as $cart){
                            array_push($send, $cart);
                         }
                        // 
                        for($i = 0; $i < count($send); ++$i){
                           
                            $dataitem = [];
                            array_push($dataitem, array(
                                 'id' => explode(",",$send[$i]->bundleid)[0],
                                 'bundlename' => explode(",",$send[$i]->bundlename)[0],
                                 'price' => explode(",",$send[$i]->price)[0],
                                 'name' => $send[$i]->name,
                                 'qty' => explode(",",$send[$i]->qty)[0],
                                 'order_id' => explode(",",$send[$i]->order_id)[0],
                                 'menuID' => explode(",",$send[$i]->menuID)
                             ));
                 
                            while($i < count($send) - 1 && $send[$i]->bundleid === $send[$i+1]->bundleid){
                                ++$i;
                                array_push($dataitem, $send[$i]);
                            }
                            array_push($dataArray,$dataitem);
                        }
                 
                 
                         return response()->json([
                             'send'=> $dataArray
                         ]);
                 

    }

    public function getCartItems($order_id){
       
        $items = [];
        $cartItems = DB::table('carts')
                    ->join('menus', 'menus.menuID','=','carts.menuID')
                    ->select('menus.name','menus.price','carts.qty','carts.order_id','carts.id','carts.menuID','carts.bundleid')
                    ->where('bundleid',NULL)
                    ->where('order_id',$order_id)
                    ->get();
        foreach($cartItems as $cart){
            array_push($items, $cart);
        }
      

        return response()->json([
            'items'=> $items
        ]);

    }
   public function getAllCartItems($order_id){
    $items = [];
    $cartItems = DB::table('carts')
                ->select('carts.*')
                ->where('order_id',$order_id)
                ->get();
    foreach($cartItems as $cart){
        array_push($items, $cart);
    }
  

    return response()->json([
        'items'=> $items
    ]);
   }
    public function updateQty($id,Request $request){
        $detail= Cart::find($request->id);
        $detail->qty = $request->qty;
        $detail->save();
       
        return response()->json([
            'message' => 'Quantity updated!'
        ]);
     }
     public function saveToTemporaryKitchenTable(Request $request){
        $data = $request->all();
        $sendToOrderDetails = [];
        $sendToKitchenRecords = [];

        for($i = 0; $i < count($data) ; $i++){
            if($data[$i]['bundleid'] == null){
                for($q=$data[$i]['qty'];$q;$q--){
                    array_push($sendToKitchenRecords, array(
                        'bundleid'=> $data[$i]['bundleid'],
                        'order_id'=> $data[$i]['order_id'],
                        'menuID'=> $data[$i]['menuID'],
                        'orderQty' => 1,
                        'status'=> 'waiting'
                       
                        ));
                }
                array_push($sendToOrderDetails,array(
                    'bundleid'=> $data[$i]['bundleid'],
                    'order_id'=> $data[$i]['order_id'],
                    'menuID'=> $data[$i]['menuID'],
                    'subtotal' => $data[$i]['price']* $data[$i]['qty'],
                    'orderQty' => $data[$i]['qty'],
                    'qtyServed'=> $data[$i]['qty'],
                    'status'=> 'waiting'
                    ));
            }else{
                $bundles = DB::table('bundle_details')
                                ->select('menuID','bundleid','qty')
                                ->where('bundleid',$data[$i]['bundleid'])->get();
                foreach($bundles as $bundle){
                    for($q= $data[$i]['qty'] * $bundle->qty ;$q;$q--){
                array_push($sendToKitchenRecords,array(
                    'bundleid'=> $bundle->bundleid,
                    'order_id'=> $data[$i]['order_id'],
                    'menuID'=> $bundle->menuID,
                    'orderQty' => 1,
                    'status'=> 'waiting'
                    ));
                }
                array_push($sendToOrderDetails,array(
                    'bundleid'=> $bundle->bundleid,
                    'order_id'=> $data[$i]['order_id'],
                    'menuID'=> $bundle->menuID,
                    'subtotal' => $data[$i]['price']* $data[$i]['qty'],
                    'orderQty' => $data[$i]['qty'],
                    'qtyServed'=> $bundle->qty * $data[$i]['qty'], 
                    'status'=> 'waiting'
                    ));
                }
            }
        }
        Kitchen::insert($sendToKitchenRecords);
        OrderDetail::insert($sendToOrderDetails);

        return response()->json([
            'sendToKitchenRecords' => $sendToKitchenRecords,
            'sendToOrderDetails' => $sendToOrderDetails
        ]);
     
     }
    // public function saveToTemporaryKitchenTable(Request $request){
    //     for($i= 0; $i < $request->orderQty; $i++){
    //     $kitchenorders = new Kitchen();
    //     $kitchenorders->orderQty = 1;
    //     $kitchenorders->menuID = $request->menuID;
    //     $kitchenorders->order_id = $request->order_id;
    //     $kitchenorders->status = $request->status;
    
    //     $kitchenorders->save();
    //     }

    //     return response()->json([
    //     'message' => 'Saved to kitchen'
    //        // $request->orderQty
    //     ]);
    // }
    public function getKitchenOrders(){
        $orders = DB::table('kitchenrecords')
        ->join('orders','orders.order_id','=','kitchenrecords.order_id')
        ->join('menus','menus.menuID','=','kitchenrecords.menuID')
        ->join('sub_categories','menus.subcatid','=','sub_categories.subcatid')
        ->join('categories','sub_categories.categoryid','=','categories.categoryid')
        ->where('categories.categoryname','!=', 'Drinks')
        ->where('kitchenrecords.status','=','waiting')
        ->orderBy('kitchenrecords.date_ordered','asc')->get();

        return response()->json([
            'orders' => $orders
        ]);
    }
    public function getDrinkWaitingOrders(){
        $orders = DB::table('kitchenrecords')
        ->join('orders','orders.order_id','=','kitchenrecords.order_id')
        ->join('menus','menus.menuID','=','kitchenrecords.menuID')
        ->join('sub_categories','menus.subcatid','=','sub_categories.subcatid')
        ->join('categories','sub_categories.categoryid','=','categories.categoryid')
        ->where('categories.categoryname','=', 'Drinks')
        ->where('kitchenrecords.status','=','waiting')
        ->orderBy('kitchenrecords.date_ordered','asc')->get();

        return response()->json([
            'orders' => $orders
        ]);
    }
    public function getDrinkPrepareOrders(){
        $orders = DB::table('kitchenrecords')
        ->select('kitchenrecords.updated_at AS updated_at','menus.name AS name','orders.order_id AS order_id','kitchenrecords.id AS id')
        ->join('orders','orders.order_id','=','kitchenrecords.order_id')
        ->join('menus','menus.menuID','=','kitchenrecords.menuID')
        ->join('sub_categories','menus.subcatid','=','sub_categories.subcatid')
        ->join('categories','sub_categories.categoryid','=','categories.categoryid')
        ->where('categories.categoryname','=', 'Drinks')
        ->where('kitchenrecords.status','=','preparing')
        ->orderBy('kitchenrecords.updated_at','asc')->get();

        return response()->json([
            'orders' => $orders
        ]);
    }
    public function removeFromKitchenOrders($id){
        $orderid = DB::table('kitchenrecords')->where('id',$id)->delete();
       
           return response()->json([
               'message' => 'Successfully deleted!'
              // 'message' => $request
           ]);
    }
   
    public function served($id){
        $status = Kitchen::find($id);
        $status->status = 'served';
        $status->save();

        return response()->json([
            'message' => 'status updated'
        ]);
    }
   public function finish($id){
       $status =Kitchen::find($id);
       $status->status= 'for serving';
       $status->save();

       return response()->json([
        'message' => 'Status updated to for serving'
       ]);
   }
    public function prepare($id){
        $status = Kitchen::find($id);
        $status->status = 'preparing';
        $status->save();

        return response()->json([
            'message' => 'status updated'
        ]);
    }
    public function isPreparing($id){
        $preparing = false;
        $status = DB::table('kitchenrecords')
                ->where('id',$id)
                ->where('status','=','preparing')
                ->count();
        if($status === 1){
            $preparing = true;
        }
        return response()->json([
            'status' => $preparing
        ]);
    }
    public function getAllPreparedItems(){
        $orders = DB::table('kitchenrecords')
        ->select('kitchenrecords.updated_at AS updated_at','menus.name AS name','orders.order_id AS order_id','kitchenrecords.id AS id')
        ->join('orders','orders.order_id','=','kitchenrecords.order_id')
        ->join('menus','menus.menuID','=','kitchenrecords.menuID')
        ->join('sub_categories','menus.subcatid','=','sub_categories.subcatid')
        ->join('categories','sub_categories.categoryid','=','categories.categoryid')
        ->where('categories.categoryname','!=', 'Drinks')
        ->where('kitchenrecords.status','=','preparing')
        ->orderBy('kitchenrecords.updated_at','asc')->get();

        return response()->json([
            'orders' => $orders
        ]);
    }
    public function servingStatusByTableNo($tableno){
       
        $orders = DB::table('kitchenrecords')
        ->select('menus.name','kitchenrecords.status','kitchenrecords.orderQty','kitchenrecords.id','orders.tableno')
        ->join('orders','orders.order_id','=','kitchenrecords.order_id')
        ->join('menus','menus.menuID','=','kitchenrecords.menuID')
        ->join('sub_categories','menus.subcatid','=','sub_categories.subcatid')
        ->join('categories','sub_categories.categoryid','=','categories.categoryid')
        ->where('tableno',$tableno)
        ->where('kitchenrecords.status','for serving')
        ->orderBy('kitchenrecords.updated_at','asc')->count();
 
         return response()->json([
             'counter' => $orders
         ]);
    
    }
    public function getAllCompleteList(){
        $orders = DB::table('kitchenrecords')
        ->select('kitchenrecords.updated_at AS updated_at','menus.name AS name','orders.order_id AS order_id','kitchenrecords.id AS id')
        ->join('orders','orders.order_id','=','kitchenrecords.order_id')
        ->join('menus','menus.menuID','=','kitchenrecords.menuID')
        ->join('sub_categories','menus.subcatid','=','sub_categories.subcatid')
        ->join('categories','sub_categories.categoryid','=','categories.categoryid')
        ->where('categories.categoryname','!=', 'Drinks')
        ->where('kitchenrecords.status','=','for serving')
        ->orderBy('kitchenrecords.updated_at','desc')->get();

        return response()->json([
            'orders' => $orders
        ]);
    }
    public function getAllCompleteDrinks(){
        $orders = DB::table('kitchenrecords')
        ->select('kitchenrecords.updated_at AS updated_at','menus.name AS name','orders.order_id AS order_id','kitchenrecords.id AS id')
        ->join('orders','orders.order_id','=','kitchenrecords.order_id')
        ->join('menus','menus.menuID','=','kitchenrecords.menuID')
        ->join('sub_categories','menus.subcatid','=','sub_categories.subcatid')
        ->join('categories','sub_categories.categoryid','=','categories.categoryid')
        ->where('categories.categoryname','=', 'Drinks')
        ->where('kitchenrecords.status','=','for serving')
        ->orderBy('kitchenrecords.updated_at','desc')->get();

        return response()->json([
            'orders' => $orders
        ]);
    }
    public function getBundleOrderQty(Request $request){
        $qty = DB::table('carts')->select('qty')
                ->where('bundleid',$request->bundleid)
                ->orWhere('id',$request->id)
                ->get();
        foreach($qty as $row){
        $row->qty = explode(",",$row->qty)[0];
        }
       return response()->json([
            'qty' => $row->qty
        ]);

    }
    public function addOrderQty(Request $request){
        $qty = DB::table('carts')->select('qty')
            ->where('bundleid',$request->bundleid)
            ->orWhere('id',$request->id)->get();

       return response()->json([
            'qty' => $row->qty
        ]);

    }
 
}


