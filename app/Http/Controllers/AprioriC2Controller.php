<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Helper\AprioriHelper;
//use App\Http\Requests;
//use App\Http\Response;
use App\Apriori;
use DB;
//use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

//use Illuminate\Support\Str;

class AprioriC2Controller extends Controller 
{
    public function generateAprioriPage(){
        $user = Auth::user();
        $userFname=$user->empfirstname;
        $userLname=$user->emplastname;
        $userImage=$user->image;
        $allMenus = Menu::all();
        $ItemSets=DB::table('apriori')
        ->selectRaw('COUNT(menuID) as count')
        ->groupBy('groupNumber')
        ->distinct()
        ->get();
        $suggestedMenus = DB::table('apriori')
        ->join('menus','apriori.menuID','=','menus.menuID')
       ->selectRaw('group_concat(menus.menuID) as menuID')
       //->select('group_concat(bundle_menus.menuID)','menus.name')
     //   ->select('bundle_menus.bundleGroup','menus.name','bundle_menus.bundleGroup')
        ->groupBy('apriori.groupNumber')
        ->get();
        $result = [];
        foreach($suggestedMenus as $row) {
            $result[] = explode(',',$row->menuID);
        }
          $sMenus=array_values($result);
        return view('admin.generateApriori',compact('userImage','userFname','userLname','allMenus','sMenus','ItemSets'));
        

    }
    public function GenerateRecommendations() {
        $samples=$this->getTransactions();
        $sc=$this->getSupportandConfidence();
        if(count($sc)==0){
            $support='';
            $confidence='';
            $support=75/100;
            $confidence=75/100;
            $apriori = new AprioriHelper($samples, $support, $confidence );
            $pairs = $apriori->all_pairs();
            $this->addPairs($pairs);
            //dd($pairs);
            return redirect('dashboard');
    }
        else{
        $support='';
        $confidence='';
        foreach($sc as $row){
        $support=$row->support/100;
        $confidence=$row->confidence/100;
        }
        $apriori = new AprioriHelper($samples, $support, $confidence );
    
        // $samples = [
        //     ['Onion', 'Potato','Burger'],
        //     ['Potato','Burger', 'Milk'],
        //     ['Milk','Beer'],
        //     ['Onion', 'Potato', 'Milk'],
        //     ['Onion', 'Potato','Burger','Beer'],
        //     ['Onion', 'Potato','Burger', 'Milk'],
        // ];
      //  $samples=$this->getTransactions();
        //$apriori = new Apriori($samples, 0.50, 0.50 );
        //$this->parr($apriori->get_rules());
        $pairs = $apriori->all_pairs();
       // print_r($pairs);
    //   $this->parr($pairs);
        $this->addPairs($pairs);
      //  print_r($this->sendApriori());

        // foreach($pairs as $pair) {
        //     $this->parr($pair);
        // }
        //$t=$apriori->predict($tests);
    // dd($pairs);
       return redirect('dashboard');
        }
        
    }

    private function addPairs($pairs) {
        $checkDB=DB::table('apriori')->get();
        if($checkDB==NULL){
        $groupNumber = 0;
        for($i=2;$i<=count($pairs);$i++) {
            foreach($pairs[$i] as $group_number) {
                ++$groupNumber;
                foreach($group_number as $menu_id) {
                    $row = array('menuID'=>$menu_id,'groupNumber'=>$groupNumber);
                    $this->addBundleRow($row);
                }
            }
        }
    }
    else{
        DB::table('apriori')->truncate();
        $groupNumber = 0;
        for($i=2;$i<=count($pairs);$i++) {
            foreach($pairs[$i] as $group_number) {
                ++$groupNumber;
                foreach($group_number as $menu_id) {
                    $row = array('menuID'=>$menu_id,'groupNumber'=>$groupNumber);
                    $this->addBundleRow($row);
                }
            }
        }

    }
}

    private function addBundleRow($row) {
        DB::table('apriori')->insert($row);
    }

    private function parr($arr) {
        echo "<pre>";
        print_r(json_encode($arr));
        echo "</pre>";
    }

    private function getTransactions() {
       
        $transactions = DB::table('order_details')
                            ->select(DB::raw('group_concat(menuID) as transaction'))
                            ->groupBy('order_id')
                            ->orderBy('order_id')
                            ->get();
        //$this->parr($transactions);
        $result = [];
        foreach($transactions as $row) {
            $result[] = explode(',',$row->transaction);
        }
        return array_values($result);
      
    }
    private function getSupportandConfidence(){
        $sc=DB::table('apriorisettings')->get();
        return $sc;
     }

    public function sendApriori(Request $request){
        $menu = [];
        $send = [];
       
        $transactions = DB::table('apriori')
            ->join('menus','apriori.menuID','=','menus.menuID')
            ->selectRaw('group_concat(menus.name) as name')
            ->selectRaw('group_concat(menus.menuID) as menuID') 
            ->groupBy('apriori.groupNumber')
            ->having('menuID',$request->menuID)
            ->get();
      

        foreach($transactions as $row){
            $menu[]=explode(",",$row->menuID);
        }
        for($i=0;$i<count($menu);$i++){
            foreach($menu[$i] as $Smenus){
                if($Smenus != $request->menuID){
                  $send[]=$Smenus;
                }
            }
           
        }
        $final = array_unique($send);
        $send = [];
        $data;
        foreach($final as $row){
            $data = DB::table('menus')->where('menuID',$row)->get();
            array_push($send,$row);
        }
        $data = [];
        for($i = 0; $i < count($send) ; $i++){
            $t = DB::table('menus')->where('menuID',$send[$i])->get();   
          
           foreach( $t as $a ){
            $localFileName  = public_path().'/menu/menu_images/'.$a->image;
            $fileData = file_get_contents($localFileName);
            $ImgfileEncode = base64_encode($fileData);
                array_push($data, array(
                    'name' => $a->name,
                    'menuID' => $a->menuID,
                    'image' => $ImgfileEncode
                ));
           }
           
        }

     return response()->json(['menu'=>$data]);
     }
    // public function sendApriori(Request $request){
    //     $data = DB::table('apriori')->where('id',$request->id)->get();

    //     return response()->json([
    //         'data' => $data
    //     ]);
    // } 


}
