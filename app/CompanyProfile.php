<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
}
