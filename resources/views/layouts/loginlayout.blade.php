<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CaterU</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('/login-signup/fonts/material-icon/css/material-design-iconic-font.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{asset('/login-signup/css/style.css')}}">
</head>
<body>
  @yield('content')
  <script src="{{asset('/login-signup/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/login-signup/js/main.js')}}"></script>
</body>
</html>
