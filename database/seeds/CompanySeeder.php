<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      
        $companies= [
            [
                'id' =>  1,
                'companyname'  =>  'Magestic Restaurant',
                'address'  =>  'Lapu Lapu Cebu City',
                'contactNo'  =>  '09123456789',
                'email'  => 'samplecompany@gmail.com',
                'tin' => '218479030-0005'
            ],

        ];
        DB::table('companies')->insert($companies);
    }
}
